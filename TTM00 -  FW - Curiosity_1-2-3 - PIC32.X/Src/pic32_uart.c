//#define _SUPPRESS_PLIB_WARNING

#include "xc.h"
#include "plib.h"
#include "../Inc/pic32_uart.h"
#include "stdio.h"


unsigned char  U1TxBuf[U1TxBufSize];
unsigned char	U1RxBuf[U1RxBufSize];
unsigned int	U1RxHead=0,U1RxTail=0;
unsigned int	U1RxCharCount=0;
unsigned char U1Unread = 0 ;

void UART1_init(unsigned long int UART_Baud)
{
    //PIC32MX470 uses PPS, so UART1 by default is NOT wired to RPF0 / RPF1
#if defined(__32MX470F512H__)
    CFGCONbits.IOLOCK = 0;
    
    U1RXRbits.U1RXR = 0b0100; // RPF1
    RPF0Rbits.RPF0R = 3;  /* RPn tied to U1TX */ 
    
    CFGCONbits.IOLOCK = 1;
#endif
    
    UARTConfigure(UART1, UART_ENABLE_PINS_TX_RX_ONLY);
	UARTSetFifoMode(UART1, UART_INTERRUPT_ON_TX_NOT_FULL | UART_INTERRUPT_ON_RX_NOT_EMPTY);
	UARTSetLineControl(UART1, UART_DATA_SIZE_8_BITS | UART_PARITY_NONE | UART_STOP_BITS_1);
	UARTSetDataRate(UART1, PBCLK_FREQUENCY, UART_Baud);
	UARTEnable(UART1, UART_ENABLE_FLAGS(UART_PERIPHERAL | UART_RX | UART_TX));

	// Configure UART RX Interrupt
	INTEnable(INT_SOURCE_UART_RX(UART1), INT_ENABLED);
	INTSetVectorPriority(INT_VECTOR_UART(UART1), INT_PRIORITY_LEVEL_2);
	INTSetVectorSubPriority(INT_VECTOR_UART(UART1), INT_SUB_PRIORITY_LEVEL_0);
}

void UART1_reset_rx_buffer(void)
{
    U1RxTail = 0;
}

unsigned char UART1_is_unread(void)
{
    return (U1Unread);
}

void UART1_clear_unread(void)
{
    U1Unread = 0;
}

void UART1_write_string(const char *string)
{
	while(*string != '\0')
	{
		while(!UARTTransmitterIsReady(UART1))	;
		UARTSendDataByte(UART1, *string);
		string++;
		while(!UARTTransmissionHasCompleted(UART1));
	}
}

#ifdef DEBUG_MODE

void UART1_write_char(const char ch)
{
		while(!UARTTransmitterIsReady(UART1))	
            continue;
		UARTSendDataByte(UART1, ch);
        UARTSendDataByte(UART1, i);
}

#else
void UART1_write_char(const char ch)
{
		while(!UARTTransmitterIsReady(UART1))	
            continue;
		UARTSendDataByte(UART1, ch);
}

#endif

void UART1_get_string(void)
{
    if(UARTReceivedDataIsAvailable(UART1))
    { 
       // U1Unread = 1;
        U1RxBuf[U1RxTail] = UARTGetDataByte(UART1);
        if(U1RxBuf[U1RxTail] == '\r')
        {   
            U1Unread = 1;
            U1RxTail = 0;
        }else
            U1RxTail++;
        if(U1RxTail >= U1RxBufSize) U1RxTail = 0;
		
    }
}

void __ISR(_UART1_VECTOR, IPL4SOFT) IntUart1Handler(void)
{
	if(INTGetFlag(INT_SOURCE_UART_RX(UART1))) //RX?
	{
        U1RxBuf[U1RxTail] = UARTGetDataByte(UART1);
		if(U1RxBuf[U1RxTail] == '\r')
        {   
            U1Unread = 1;
            U1RxTail = 0;
        }else
            U1RxTail++;
        if(U1RxTail >= U1RxBufSize) U1RxTail = 0;
		INTClearFlag(INT_SOURCE_UART_RX(UART1));
	}

	
	if (INTGetFlag(INT_SOURCE_UART_TX(UART1)))
	{
		INTClearFlag(INT_SOURCE_UART_TX(UART1));
	}
}