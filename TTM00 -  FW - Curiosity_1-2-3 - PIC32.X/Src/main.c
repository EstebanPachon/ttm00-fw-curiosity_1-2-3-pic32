/* 
 * File:   main.c
 * Author: NB2
 *
 * Created on July 10, 2019, 12:24 AM
 */
#define FREQUENCY (96000000)
#define PBCLK_FREQUENCY (96 * 1000 * 1000)
#define TOGGLES_PER_SEC 1   
#define CORE_TICK_RATE (FREQUENCY / 2 / TOGGLES_PER_SEC)   

#define PINS_CREE BIT_2|BIT_3|BIT_10
#define PINS_LEDS BIT_4|BIT_6|BIT_7
#define PIN_SW BIT_6


#include "main.h"

void __ISR(_CORE_TIMER_VECTOR,ipl2) CoreTimerHandler(void);
void LED_on(uint8_t LED);
void LED_off(uint8_t LED);
void LED_toggle(uint8_t LED);

void main(void){
           
    SYSTEMConfigPerformance(FREQUENCY);
    OpenCoreTimer(CORE_TICK_RATE);
    mConfigIntCoreTimer(CT_INT_ON | CT_INT_PRIOR_2 | CT_INT_SUB_PRIOR_1);
    INTEnableSystemMultiVectoredInt(); 
    //mEnableIntCoreTimer();
            
    mPORTBSetPinsDigitalOut(PINS_CREE);
    mPORTBClearBits(PINS_CREE);
    mPORTESetPinsDigitalOut(PINS_LEDS);
    mPORTESetBits(PINS_LEDS);
           
    while(1);
         
}

void __ISR(_CORE_TIMER_VECTOR,ipl2) CoreTimerHandler(void){
    mCTClearIntFlag();
    UpdateCoreTimer(CORE_TICK_RATE);
    mPORTEToggleBits(PINS_LEDS);  
}

void LED_on(uint8_t LED){
    switch (LED){
        case LED_1:
            mPORTESetBits(BIT_4);
            break;
        case LED_2:
            mPORTESetBits(BIT_6);
            break;
        case LED_3:
            mPORTESetBits(BIT_7);
            break;
        default:
            break;                   
    }
}

void LED_off(uint8_t LED){
    switch (LED){
        case LED_1:
            mPORTEClearBits(BIT_4);
            break;
        case LED_2:
            mPORTEClearBits(BIT_6);
            break;
        case LED_3:
            mPORTEClearBits(BIT_7);
            break;
        default:
            break;
    }
}

void LED_toggle(uint8_t LED){
    switch (LED){
        case LED_1:
            mPORTEToggleBits(BIT_4);
            break;
        case LED_2:
            mPORTEToggleBits(BIT_6);
            break;
        case LED_3:
            mPORTEToggleBits(BIT_7);
            break;
        default:
            break;
    }   
}

